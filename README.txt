/*******************README**********************/
1.-Millores en el codic com sols fer una consulta per a cada modul.
2.-Si el contador veu que sols es un producte ejecuta el details del producte, si es més de uno el bootpage, a més del modal per a veure mes informació.
3.-He fet que si pulses en el list dels productes del map angular agarre la id la envie al controlador i torna a ejecutar el loadMap pero esta volta enviant una id que mitjansant un forEach agarre la adecuada i despenent de la id óbric el marcador correcta a eixa id.
4.-També en el map tens un input que desplega un autocomplete de un json amb les ciutats d'Espanya i depenent de quina selecciones agafa la seua latitud i longitud i li pase al loadMap redirigin a la localització buscada.
5.-He fet el create del CRUD.
6.-Ingual que en el proyecte anterior gaste una api espesífica per al meu projecte.
7.-Envés de utilitzar el teu datepicker que tens en directiva gaste el que oferis angular(en el create), en el profile el teu en directiva.
8.-Tinc prous factories de serveis propies repartides per tota la aplicació.
9.-Encripte el token que guarde en localStorage amb l'opció de fero amb Base_64 o amb btoa/atob mitjansant serveis.
10.-He creat un carrito on pots comprar els productes, triar la cuantitat, he insertarlos en una tabla una volta pulses en comprar.
11.-També he creat una secció de lista de deseos.