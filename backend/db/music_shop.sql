-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 11-06-2018 a las 08:55:19
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `music_shop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albums`
--

CREATE TABLE `albums` (
  `discname` varchar(100) DEFAULT NULL,
  `discid` int(100) NOT NULL,
  `discprice` varchar(100) DEFAULT NULL,
  `date_entry` varchar(100) DEFAULT NULL,
  `date_expiration` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `valoration` varchar(100) DEFAULT NULL,
  `discdesc` varchar(200) DEFAULT NULL,
  `artist` varchar(200) DEFAULT NULL,
  `genre` varchar(200) DEFAULT NULL,
  `subgenre` varchar(200) DEFAULT NULL,
  `discpic` varchar(200) DEFAULT NULL,
  `n_views` int(11) DEFAULT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `albums`
--

INSERT INTO `albums` (`discname`, `discid`, `discprice`, `date_entry`, `date_expiration`, `format`, `valoration`, `discdesc`, `artist`, `genre`, `subgenre`, `discpic`, `n_views`, `lat`, `lng`, `type`) VALUES
('NWA GREATEST HITS', 10000, '25', '23/03/2018', '28/04/2018', 'CD Vinyl ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Aerosmith', 'Hip-Hop', 'Gangsta Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/27375-1198223285_f.jpg', 23, 38.821426, -0.614682, 'album'),
('WAR AND PEACE', 10001, '24', '09/05/2018', '13/06/2018', 'CD Cassete ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Buckcherry', 'Dance', 'Chillstep', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/8445-31745-450xN.jpg', 5, 38.852814, -0.380449, 'album'),
('Out of the blue', 10003, '43', '23/03/2018', '26/04/2018', 'Vinyl Mp3 Cassete ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Blue Sky Black Death', 'Rock', 'Surf', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/3303-81ZacJvsDxL._SY355_.jpg', 5, 39.188911, -0.238382, 'album'),
('I need a dollar', 10004, '21', '31/03/2018', '28/04/2018', 'CD Vinyl Mp3 ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Tom Waits', 'Rock', 'Art Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/4244-a1ab1737b92111b7426683e5075483d5--pink-floyd-album-cover.jpg', 2, 38.340240, -0.537827, 'album'),
('Good things', 10005, '32', '23/03/2018', '26/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Miles Davis', 'R&B', 'Modern Soul', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/2556-hqdefault.jpg', 2, 39.464184, -0.381821, 'album'),
('Way dont we go', 10006, '23', '23/03/2018', '26/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Dream Theater', 'Rock', 'Cock Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14777-71hCNQgkhzL._SY355_.jpg', 2, 38.994064, -1.855078, 'album'),
('Come Down', 10007, '32', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Skid Row', 'Alternative', 'Grunge', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/8702-descarga.jpg', 1, 38.483330, -0.814673, 'album'),
('Gladiator OST', 10008, '24', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 Cassete ', 'Very good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Fleetwood Mac', 'Soundtrack', 'Movie Soundtrack', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/22333-60756.jpg', 3, 38.267147, -0.719092, 'album'),
('DNA Problems', 10009, '26', '23/03/2018', '21/04/2018', 'CD Vinyl ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Buckcherry', 'Hip-Hop', 'Alternative Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14469-la-et-ms-kendrick-lamars-to-pimp-a-butterfly-dense-difficult-and-grandoise-20150316-399x400.jpg', 3, 37.976746, -1.128883, 'album'),
('MAAD City', 10010, '34', '23/03/2018', '21/04/2018', 'CD Cassete ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Alpha Blondy', 'Hip-Hop', 'West Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/28883-51Zzc7PUDML._SY355_.jpg', 1, 42.687363, -2.997385, 'album'),
('GREATEST HITS 2PAC', 10012, '28', '31/03/2018', '28/04/2018', 'CD Vinyl Mp3 ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'DJ Shadow', 'Hip-Hop', 'East Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18247-hqdefault.jpg', 4, 38.817936, -0.610957, 'album'),
('Gemini', 10021, '23', '27/03/2018', '26/04/2018', 'CD Vinyl ', 'Good', 'Gemini is Macklemore first album without Ryan Lewis since the release of his solo album The Language of My World. In an interview with Rolling Stone, Macklemore said of the album, Its not extremely po', 'Dire Straits', 'Hip-Hop', 'Alternative Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/854-macklemore-gemini-cd-clean-version.jpg', 11, 42.568668, -0.545454, 'album'),
('I AM THE WEST', 10023, '15', '23/03/2018', '28/04/2018', 'CD ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Snoop Dogg', 'Blues', 'Delta Blues', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/12562-hqdefault.jpg', 13, 38.820034, -0.606305, 'album'),
('The house rules', 10032, '32', '24/03/2018', '28/04/2018', 'CD ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Mark Knopfler', 'Country', 'Outlaw Country', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/24846-christian-kane-house-rules-UCB.jpg', 1, 41.993874, -4.526682, 'album'),
('Zapatillas', 10035, '23', '24/03/2018', '27/04/2018', 'CD Mp3 ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Dire Straits', 'Pop', 'Pop/Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18223-descarga.jpg', 0, 38.537830, -0.101663, 'album'),
('Cloud mine', 10036, '24', '24/03/2018', '28/04/2018', 'CD Mp3 ', 'Bad', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Warren G', 'Electronic', 'Basseline', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/20796-32105690_350_350.jpg', 0, 39.133450, -0.471902, 'album'),
('My type', 10038, '21', '30/03/2018', '28/04/2018', 'CD Vinyl ', 'Good', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Rage Against the Machine', 'Pop', 'Britpop', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/26-51UY4lPdbwL._SX355_.jpg', 0, 39.193928, -2.160500, 'album'),
('Aleja', 12321, '12', '05-05-2018', '03-06-2018', 'Mp3 ', 'Good', 'sadsadsadsadas', 'Alpha Blondy', 'Alternative', 'Crossover Trash', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/default-avatar.jpg', 0, 0.000000, 0.000000, ''),
('Welcome to the jungle', 23456, '21', '23/03/2018', '25/04/2018', 'CD Vinyl ', 'Good', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Rage Against the Machine', 'Rock', 'Hard Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/17375-f3d22077005b4621425731c9a5a17e0e.jpg', 0, 40.412170, -3.709850, 'album'),
('Aleja', 34324, '12', '05-05-2018', '02-06-2018', 'Vinyl ', 'Good', 'asdsadsadsadsad', 'Aerosmith', 'Alternative', 'Aleternative Rock', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/default-avatar.jpg', 0, 0.000000, 0.000000, ''),
('Fire and the flood', 34521, '23', '28/03/2018', '21/04/2018', 'CD Mp3 ', 'Bad', 'Have a bunch of buttons that all trigger the same modal with slightly different contents? Use event.relatedTarget and HTML data-* attributes (possibly via jQuery) to vary the contents of the modal dep', 'Mr. Big', 'Alternative', 'Indie Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/24633-2092-268x0w.jpg', 2, 39.126629, -0.525735, 'album'),
('Aleja', 43535, '12', '05-05-2018', '03-06-2018', 'Mp3 ', 'Good', 'saadsadsadsad', 'Joe Satriani', 'Blues', 'Chicago Blues', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/default-avatar.jpg', 0, 0.000000, 0.000000, ''),
('Sing me to sleep', 65748, '43', '23/03/2018', '27/04/2018', 'CD Vinyl Mp3 ', 'Very good', 'Some text to enable scrolling.. Lorem ipsum dolor sit amet, illum definitiones no quo, maluisset concludaturque et eum, altera fabulas ut quo. Atqui causae gloriatur ius te, id agam omnis evertitur eu', 'Joe Satriani', 'Electronic', 'Electronic Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/14453-faded.jpg', 1, 39.033672, -0.874001, 'album'),
('Aleja', 76567, '12', '12-05-2018', '10-06-2018', 'Vinyl ', 'Good', 'wdqweqweqw', 'Joe Satriani', 'Blues', 'Electronic Blues', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/default-avatar.jpg', 0, 0.000000, 0.000000, ''),
('ASDSAD', 87651, '32', '13-05-2018', '03-06-2018', 'Vinyl ', 'Good', 'XFDXDDFXFDFDD', 'Dream Theater', 'Hip-Hop', 'East Coast Rap', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/default-avatar.jpg', 0, 0.000000, 0.000000, ''),
('Be As You Are', 90874, '25', '05/04/2018', '25/05/2018', 'CD ', 'Very good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Jimi Hendrix', 'Rock', 'Cock Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/18584-13138789_10154068840956215_4844389042469885217_n.jpg', 1, 38.513424, -1.693581, 'album'),
('Recover', 90875, '20', '04/04/2018', '24/05/2018', 'CD ', 'Good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Snoop Dogg', 'Hip-Hop', 'West Coast Rap', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/15667-descarga.jpg', 1, 38.874420, -1.108009, 'album'),
('Who killed Matt Maeson', 90876, '14', '04/04/2018', '24/05/2018', 'CD ', 'Good', 'Matriz redundante de discos independientes es una tecnología de almacenamiento que combina varios discos duros en una sola unidad lógica para proporcionar tolerancia a fallos.', 'Dream Theater', 'Alternative', 'Collage Rock', '/workspace/FW_PHP_MVC_OO_JS_JQuery/media/4139-0006465070_350.jpg', 2, 38.485050, -2.401100, 'album');

--
-- Disparadores `albums`
--
DELIMITER $$
CREATE TRIGGER `music_shop` BEFORE INSERT ON `albums` FOR EACH ROW BEGIN
	IF NEW.discprice <= 0
		THEN SET NEW.discprice = 5;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating`
--

CREATE TABLE `rating` (
  `ID` mediumint(9) NOT NULL,
  `token` varchar(100) NOT NULL,
  `idProd` int(11) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rating`
--

INSERT INTO `rating` (`ID`, `token`, `idProd`, `rating`) VALUES
(1, 'Vere71fd31c2fdf956477abfdaff4f74613', 10003, 5),
(2, 'Vere71fd31c2fdf956477abfdaff4f74613', 10004, 2),
(3, 'Vere71fd31c2fdf956477abfdaff4f74613', 23456, 1),
(4, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10001, 3),
(6, 'Verdbbdcbd3b6087ddfc96710b622e87813', 34324, 1),
(22, 'Vere71fd31c2fdf956477abfdaff4f74613', 10008, 5),
(37, 'Vere71fd31c2fdf956477abfdaff4f74613', 10001, 5),
(41, 'Vere71fd31c2fdf956477abfdaff4f74613', 10012, 5),
(42, 'Vere71fd31c2fdf956477abfdaff4f74613', 10006, 5),
(43, 'Vere71fd31c2fdf956477abfdaff4f74613', 65748, 5),
(48, 'Vere71fd31c2fdf956477abfdaff4f74613', 10007, 5),
(49, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10012, 5),
(50, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10003, 5),
(51, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10003, 5),
(52, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10004, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shop`
--

CREATE TABLE `shop` (
  `id` int(11) NOT NULL,
  `user` varchar(100) DEFAULT NULL,
  `prod` varchar(60) DEFAULT NULL,
  `price` varchar(60) DEFAULT NULL,
  `cant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `shop`
--

INSERT INTO `shop` (`id`, `user`, `prod`, `price`, `cant`) VALUES
(9, 'Aleja', '10000', '25', 1),
(10, 'Aleja', '10003', '43', 1),
(11, 'Aleja', '10000', '25', 1),
(12, 'Aleja', '10003', '43', 1),
(15, 'Aleja', '10001', '24', 1),
(18, 'Aleja', '10003', '43', 1),
(19, 'Aleja', '10001', '24', 1),
(21, 'Aleja', '10003', '43', 1),
(22, 'Aleja', '10001', '24', 1),
(23, 'Aleja', '10003', '43', 1),
(24, 'Aleja', '10001', '24', 1),
(38, '', '10003', '43', 1),
(39, '', '10001', '24', 1),
(40, 'Aleja', '10003', '43', 3),
(41, 'Aleja', '10001', '24', 3),
(42, 'Aleja', '10003', '129', 3),
(43, 'Aleja', '10001', '72', 3),
(44, 'Aleja', '10003', '43', 1),
(45, 'Aleja', '10001', '72', 3),
(46, 'Aleja', '10003', '129', 3),
(47, 'Aleja', '10001', '96', 4),
(48, 'Aleja', '10003', '43', 1),
(49, 'Aleja', '10001', '24', 1),
(50, 'Aleja', '10003', '172', 4),
(51, 'Aleja', '10000', '25', 1),
(52, 'Alepla', '10001', '72', 3),
(53, 'Alepla', '10003', '86', 2),
(54, 'Alepla', '10003', '86', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `date_birthday` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user`, `email`, `name`, `lastname`, `password`, `date_birthday`, `type`, `city`, `avatar`, `active`, `token`) VALUES
('2Uke6R5Q6mRjQnE6OD9e7PvVvD83', 'aleplacambralol@gmail.com', 'Alex', 'Pla', '$2y$10$92UHc.TCU1KEc5pQD.fVg.ACPNIFWa8At6n4Z2JxEAPGIM/I/RtkC', '', 'client', '', '/workspace/FW_PHP_MVC_OO_AngularJS/backend/media/379-Ponentes.png', 1, 'Vera257c21b6b4640be79a49713f537ed40'),
('Aleja', 'aleplacambralol@gmail.com', '', '', '$2y$10$IJaUg8oYDiedtFXpnZ95b.GpY8L/4Z.rWQWGXXIj5y5orUxPrTiOq', '', 'admin', '', 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427ed41d8cd98f00b204e9800998ecf8427e?s=400&d=identicon&r=g', 1, 'Verdbbdcbd3b6087ddfc96710b622e87813'),
('Alepla', 'aleplacambra@gmail.com', '', '', '$2y$10$8nYWOQ6BRrbvY3EcWCM5bekUjC0sAMgHgdhD47V6wCO5LNzt3Lv8K', '', 'client', 'León', 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427ed41d8cd98f00b204e9800998ecf8427e?s=400&d=identicon&r=g', 1, 'Vere71fd31c2fdf956477abfdaff4f74613'),
('PS0cNVkMOtdOQKdGOZvPGxq9CoQ2', 'aleplacambra@gmail.com', '', '', '', '', 'client', 'Cantabria', 'https://lh6.googleusercontent.com/-4A3GCmIAA2w/AAAAAAAAAAI/AAAAAAAAIhU/Z9E7Cu6GHz4/photo.jpg', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wishlist`
--

CREATE TABLE `wishlist` (
  `ID` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `idProd` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wishlist`
--

INSERT INTO `wishlist` (`ID`, `token`, `idProd`) VALUES
(4, 'Vere71fd31c2fdf956477abfdaff4f74613', 10001),
(5, 'Vere71fd31c2fdf956477abfdaff4f74613', 10003),
(6, 'Verdbbdcbd3b6087ddfc96710b622e87813', 10004);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`discid`);

--
-- Indices de la tabla `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user`);

--
-- Indices de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `rating`
--
ALTER TABLE `rating`
  MODIFY `ID` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
