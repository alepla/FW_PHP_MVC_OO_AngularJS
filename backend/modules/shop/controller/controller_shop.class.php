<?php

class controller_shop {
    function __construct() {
        $_SESSION['module'] = "shop";
    }

///////////////////////////////////////////////DETAILS
    function idProduct() {
        $id = $_GET['param'];
        $arrValue = loadModel(MODEL_SHOP, "shop_model", "details_shop",$id);
        if ($arrValue) {
            $jsondata["album"] = $arrValue[0];
            echo json_encode($jsondata); 
            exit;
        }else {
            echo json_encode("error");
            exit;
        }
    }
    
    function add() {
        if ($_POST["add"]) {
            $id = $_POST["add"];
            $arrValue = loadModel(MODEL_SHOP, "shop_model", "add_one",$id);
            echo json_encode($arrValue);
            exit;
        }         
    }

//////////////////////////////////////////////LIST
    function prods() {
        $arrValue = loadModel(MODEL_SHOP, "shop_model", "list_shop");
        if ($arrValue) {
            echo json_encode($arrValue);
            exit;
        } else {
            echo json_encode("error");
            exit;
        }
    }

    function wishList() {
        $data = array(
          'token' => $_POST['token'],
          'id' => $_POST['id']
        );
        $arrValue = loadModel(MODEL_SHOP, "shop_model", "wishList", $data);
    }

    function Rating() {
        $data = array(
          'token' => $_POST['token'],
          'id' => $_POST['id'],
          'rating' => $_POST['rating']
        );
        $arrValue = loadModel(MODEL_SHOP, "shop_model", "Rating", $data);
        echo json_encode($arrValue);
    }

    function totalRating() {
        $arrValue = loadModel(MODEL_SHOP, "shop_model", "totalRating");
        echo json_encode($arrValue);
    }
}