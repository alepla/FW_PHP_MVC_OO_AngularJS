<?php

class albums_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = albums_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_albums_BLL($arrArgument){
      return $this->dao->create_album_DAO($this->db, $arrArgument);
    }

    public function obtain_artists_BLL($url){
      return $this->dao->obtain_artists_DAO($url);
    }

    public function obtain_genres_BLL(){
      return $this->dao->obtain_genres_DAO();
    }

    public function obtain_subgenres_BLL($arrArgument){
      return $this->dao->obtain_subgenres_DAO($arrArgument);
    }

    public function sele_id_BLL($id){
      return $this->dao->sele_id_DAO($this->db, $id);
    }

    public function list_all_albums_BLL(){
      return $this->dao->list_all_albums_DAO($this->db);
    }

    public function read_album_BLL($id){
      return $this->dao->read_album_DAO($this->db, $id);
    }

    public function delete_album_BLL($id){
      return $this->dao->delete_album_DAO($this->db, $id);
    }

    public function update_album_BLL($arrArgument){
      return $this->dao->update_album_DAO($this->db, $arrArgument);
    }
}
