<?php

class controller_login {
    function __construct() {
    	require_once(UTILS_LOGIN . "functions.inc.php");
        require_once(UTILS . "upload.php");
        $_SESSION['module'] = "login";
    }

    public function register_user() {
        $jsondata = array();
        $data = array(
          'user' => $_POST['nameUser'],
          'pass' => $_POST['passwordUser'],
          'pass2' => $_POST['repasswordUser'],
          'email' => $_POST['emailUser']
        );
        $result = validate_userPHP($data);
        if ($result['result']) {
            $avatar = get_gravatar($result['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
            $arrArgument = array(
                'user' => $result['data']['user'],
                'email' => $result['data']['email'],
                'pass' => password_hash($result['data']['pass'], PASSWORD_BCRYPT),
                'type' => $result['data']['type'],
                'avatar' => $avatar,
                'token' => ""
            );

            $arrValue = false;
            $arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "alta_user", $arrArgument);

            if ($arrValue) {
                sendtoken($arrArgument, "alta");
                $jsondata["success"] = true;
                $jsondata['datos'] = $arrArgument;
                echo json_encode($jsondata);
                exit();
            }else {
                $jsondata["success"] = false;
                $jsondata['error'] = $result['error'];
                //header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit();
            }
        }else {
            $jsondata["success"] = false;
            $jsondata['error'] = $result['error'];
            //header('HTTP/1.0 400 Bad error');
            echo json_encode($jsondata);
            exit();
        }
    }

    function verify() {
        if (substr($_POST['token'], 0, 3) == "Ver") {
            $argument = array(
                'token' => $_POST['token']
            );

            $value = false;
            $value = loadModel(MODEL_LOGIN, "login_model", "verify_account", $argument);

            if ($value) {
                echo json_encode("good");
            } else {
                echo json_encode("bad");
            }
        }
    }

    function access() {
        $jsondata = array();
        $login_info = array(
            'username' => $_POST['userName']
        );
        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "access_count", $login_info);
        $arrValue = password_verify($_POST['Password'], $arrValue[0]['password']);
        if ($arrValue) {
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "val_count", $login_info);
            if ($arrValue[0]['active'] == 1) {
                $info = loadModel(MODEL_LOGIN, "login_model", "select_info_user", $login_info);
                if($info){
                    $jsondata["success"] = true;
                    $jsondata['data'] = $info;
                    echo json_encode($jsondata);
                    exit();
                }else {
                    echo json_encode("error2");
                    exit();  
                }
            }else {
                echo json_encode("error1");
                exit();
            }
        }else {
            echo json_encode("error");
            exit();
        }
    }

    function recover_pass() {
        $email = $_POST['data'];
        $val = validatemail($email);

        if($val) {
            $arrValue = false;
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email", $val);
            if ($arrValue){
                $token = "Cha" . md5(uniqid(rand(), true));
                $arrArgument = array(
                    'token' => $token,
                    'email' => $email
                );
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "change_token", $arrArgument);
                if($arrValue){
                    sendtoken($arrArgument, "mod");
                    echo json_encode("good");
                }else{
                    echo json_encode("error2");
                }
            }else {
                echo json_encode("error1");
            }
        }else {
            echo json_encode("error");
        }
    }

    function change_pass() {
        $passwords = array(
            'password' => $_POST['password'],
            'passwordRepeat' => $_POST['passwordRepeat'],
        );
        $result = validate($passwords);
        if($result['result']) {
            if(substr($_POST['token'], 0, 3) == "Cha") {
                $token = $_POST['token'];
                $email = loadModel(MODEL_LOGIN, "login_model", "selectEmail", $token);
                if($email) {
                    $val = $email[0]['email'];
                    $new_pass = password_hash($_POST['password'], PASSWORD_BCRYPT);
                    $new_token = "Ver" . md5(uniqid(rand(), true));

                    $arrArgument = array(
                        'token' => $_POST['token'],
                        'new_pass' => $new_pass,
                        'new_token' => $new_token
                    );

                    $arrValue = false;
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "change_password", $arrArgument);

                    if ($arrValue) {
                        $arrValue = loadModel(MODEL_LOGIN, "login_model", "validate_email", $val);
                        if (substr($arrValue[0]['token'], 0, 3) == "Ver") {
                            echo json_encode("error1");
                        }else {
                            echo json_encode("good");
                            $arrvalue = loadModel(MODEL_LOGIN, "login_model", "change_token_2", $arrArgument);
                        }
                    }else {
                        echo json_encode("error");
                    }
                }else {
                    echo json_encode("error");
                }
            }else {
                echo json_encode("505");
            }
        }else {
            echo json_encode($result['error']);
        }        
   }

    function list_profile() {
        $username = $_POST['username'];

        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "list_user_profile", $username);

        if($arrValue) {
            echo json_encode($arrValue);
        }else {
            echo json_encode("error");
        }
    }

    function upload() {
        $profile_image = upload_files();
        $_SESSION['profile_image'] = $profile_image;
        echo json_encode($profile_image);
    }

    function delete(){
        $_SESSION['profile_image'] = array();
    }

    function update_info_prof(){
        $data = array(
          'user' => $_POST['user'],
          'name' => $_POST['nameUser'],
          'lastname' => $_POST['lastnameUser'],
          'city' => $_POST['city'],
          'date' => $_POST['date'],
          'avatar' => $_POST['avatar']
        );
        if (empty($_SESSION['profile_image'])){
            $img_profile = array('result' => true, 'error' => "", "data" => $_POST['avatar']);
        }else{
            $img_profile = $_SESSION['profile_image'];
        }
        if($_POST['date'] != ""){
            $age = validate_age($_POST['date']);
        }else{
            $age = 7;
        }
        if ($age < 6){
            echo json_encode("error1");
        }else {
            if ($img_profile['result']){
                $arrArgument = array(
                    'user' => $_POST['user'],
                    'name' => $_POST['nameUser'],
                    'lastname' => $_POST['lastnameUser'],
                    'city' => $_POST['city'],
                    'birthday' => $_POST['date'],
                    'img_profile' => $img_profile['data']
                );
                $arrValue = false;
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_user_profile", $arrArgument);
                if ($arrValue){
                    session_unset($_SESSION['profile_image']);
                    echo json_encode("good");
                }else {
                    echo json_encode("error");
                }
            }else {
                $jsondata['success'] = false;
                $jsondata['img_profile'] = $img_profile['error'];
                echo json_encode($jsondata);
            }
        }
    }

    function social(){
        $jsondata = array();
        if($_POST['email'] == null){
            echo json_encode("email_no");
        }else {
            $arrArgument = array(
              'user' => $_POST['user'],
              'name' => $_POST['name'],
              'email' => $_POST['email'],
              'type' => "client",
              'avatar' => $_POST['avatar']
            );
            $arrValue = false;
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "search_social", $arrArgument);
            if($arrValue){
                $arrValue = false;
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_social", $arrArgument);
                if($arrValue){
                    $jsondata["success"] = true;
                    $jsondata['data'] = $arrArgument;
                    echo json_encode($jsondata);
                    exit();
                }else {
                    echo json_encode("error");
                }
            }else {
                $arrValue = false;
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "insert_social", $arrArgument);
                if($arrValue){
                    $arrValue = false;
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_social", $arrArgument);
                    if($arrValue){
                        $jsondata["success"] = true;
                        $jsondata['data'] = $arrArgument;
                        echo json_encode($jsondata);
                        exit();
                    }else {
                        echo json_encode("error");
                    }
                }else {
                    echo json_encode("error");
                }
            }
        }
    }

    function selectType() {
        $token = $_GET['param'];
        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "selectType", $token);
        if($arrValue) {
            echo json_encode($arrValue);
        }else {
            echo json_encode("error");
        }
    }

    function selectTypeSocial() {
        $userID = $_GET['param'];
        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "selectTypeSocial", $userID);
        if($arrValue) {
            echo json_encode($arrValue);
        }else {
            echo json_encode("error");
        }
    }

    function loadCitys() {
        $citys_json = file_get_contents(SITE_ROOT . '/modules/map/resources/ListOfCitysSpain.json');
        echo $citys_json;
    }

    function selectWishList() {
        $jsondata = array();
        $token = $_GET['param'];
        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "selectMyWishList", $token);
        if($arrValue) {
            $jsondata["success"] = true;
            $jsondata['data'] = $arrValue;
            echo json_encode($jsondata);
        }else {
            $jsondata['success'] = false;
            echo json_encode($jsondata);
        }
    }

    function selectInfoRatings() {
        $jsondata = array();
        $token = $_GET['param'];
        $arrValue = false;
        $arrValue = loadModel(MODEL_LOGIN, "login_model", "selectRatings", $token);
        if($arrValue) {
            $jsondata["success"] = true;
            $jsondata['data'] = $arrValue;
            echo json_encode($jsondata);
        }else {
            $jsondata['success'] = false;
            echo json_encode($jsondata);
        }
    }
}