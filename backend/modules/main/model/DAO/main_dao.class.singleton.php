<?php
class main_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function page_main_DAO($db) {
        $sql = "SELECT discname, discpic, discid FROM albums ORDER BY n_views DESC";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    /*public function autocomplete_main_DAO($db) {
        $sql = "SELECT discname FROM albums ORDER BY discname";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function id_main_DAO($db,$id) {
        $sql = "SELECT discname FROM albums WHERE discid = '$id'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }*/

    public function name_main_DAO($db,$name) {
        $sql = "SELECT COUNT(discname) as cont, discid FROM albums WHERE discname LIKE '%" . $name . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }
}
