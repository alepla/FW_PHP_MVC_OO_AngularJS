<?php

class controller_main {

    function __construct() {
        $_SESSION['module'] = "main";
    }

    function page() {
        $arrValue = loadModel(MODEL_MAIN, "main_model", "page_main");
        if ($arrValue) {
            echo json_encode($arrValue);
            exit;
        } else {
            echo json_encode("error");
            exit;
        }
    }
}