<?php

class controller_contact {
  function __construct() {
      $_SESSION['module'] = "contact";
  }

  function send_contact(){
    //Mail for the user
  	$arrArgument = array(
      'type' => 'contact',
      'token' => '',
      'name' => $_POST['name_contact'],
  		'email' => $_POST['email_contact'],
  		'subject' => $_POST['subject_contact'],
  		'message' => $_POST['message_contact']
  	);
    if ($arrArgument){
      echo "ok";
      send_mail($arrArgument);
    } else {
      echo "bad";
    }

    //Mail for the admin
    $arrArgument = array(
      'type' => 'admin',
      'token' => '',
      'name' => $_POST['name_contact'],
      'email' => $_POST['email_contact'],
      'subject' => $_POST['subject_contact'],
      'message' => $_POST['message_contact']
    );
    if ($arrArgument){
      echo "ok";
      send_mail($arrArgument);
    } else {
      echo "bad";
    }
  }
}