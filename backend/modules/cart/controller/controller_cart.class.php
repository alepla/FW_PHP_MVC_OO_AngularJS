<?php

class controller_cart {
  function __construct() {
      $_SESSION['module'] = "cart";
  }

  function list_prods(){
       $arrValue = loadModel(MODEL_CART, "cart_model", "list_prods");
       echo json_encode($arrValue);
  }

  function shop() {
  	$data = array(
		'user' => $_POST['user'],
		'qty' => $_POST['qty'],
		'prod' => $_POST['prod'],
		'price' => $_POST['price']
    );
    $arrValue = loadModel(MODEL_CART, "cart_model", "shop", $data);
    echo json_encode($arrValue);
  }
}