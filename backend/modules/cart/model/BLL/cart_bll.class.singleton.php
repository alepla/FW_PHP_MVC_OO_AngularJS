<?php

class cart_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = cart_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_prods_BLL(){
      return $this->dao->list_prods_DAO($this->db);
    }

    public function shop_BLL($data){
      return $this->dao->shop_BLL($this->db, $data);
    }
}
