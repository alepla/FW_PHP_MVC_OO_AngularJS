<?php

class cart_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = cart_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_prods() {
        return $this->bll->list_prods_BLL();
    }

    public function shop($data) {
        return $this->bll->shop_BLL($data);
    }
}
