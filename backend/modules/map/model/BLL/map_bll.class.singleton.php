<?php

class map_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = map_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_map_BLL(){
      return $this->dao->list_map_DAO($this->db);
    }

    public function details_map_BLL($id){
      return $this->dao->details_map_DAO($this->db,$id);
    }
}
