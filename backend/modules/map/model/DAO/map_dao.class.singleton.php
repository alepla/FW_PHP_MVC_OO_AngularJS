<?php

class map_dao {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_map_DAO($db) {
        $sql = "SELECT * FROM albums WHERE 1";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }

    public function details_map_DAO($db, $id) {
        $sql = "SELECT discpic, discname, discprice, discdesc, artist FROM albums WHERE discname='".$id."'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
        
    }
}
