<?php

class controller_map {
    function __construct() {
        $_SESSION['module'] = "map";
    }

    function list_albums() {
        $arrValue = loadModel(MODEL_MAP, "map_model", "list_map");
        if ($arrValue){
            echo json_encode($arrValue);
            exit();
        }else{
            echo json_encode("error");
            exit();
        }
    }

    function idMap() {
        if ($_POST["idProduct"]) {
            $id = $_POST["idProduct"];
            $arrValue = loadModel(MODEL_MAP, "map_model", "details_map", $id);
            if ($arrValue) {
                $jsondata["album"] = $arrValue[0];
                echo json_encode($jsondata); 
                exit();
            }else {
                echo json_encode("error");
                exit();
            }
        } 
    }

    function autocomplete() {
        $direccion = $_GET["param"];
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        ); 
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($direccion).'&key=AIzaSyDAhlrtCRkZQAmCe13cg4fHOWDE3eBAliY', false, stream_context_create($arrContextOptions));
        $geo = json_decode($geo, true);

        if ($geo['status'] = 'OK') {
            $latitud = $geo['results'][0]['geometry']['location']['lat'];
            $longitud = $geo['results'][0]['geometry']['location']['lng'];
        }
        $cor = array('lat' => $latitud,
                     'long' => $longitud
                );

        echo json_encode($cor);
    }

    function loadCitys() {
        $citys_json = file_get_contents(SITE_ROOT . '/modules/map/resources/ListOfCitysSpain.json');
        echo $citys_json;
    }
}