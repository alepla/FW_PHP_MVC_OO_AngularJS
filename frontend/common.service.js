musicpla.factory("CommonService", ['$rootScope','$timeout', 'toastr', function ($rootScope, $timeout, toastr) {
        var service = {};
        service.amigable = amigable;
        service.autocompleteDetails = autocompleteDetails;
        return service;
        
        function amigable(url) {
            var link = "";
            url = url.replace("?", "");
            url = url.split("&");

            for (var i = 0; i < url.length; i++) {
                var aux = url[i].split("=");
                link += aux[1] + "/";
            }
            return link;
        }

        function autocompleteDetails(name, shop){
            var cont = 0;
            var arrayMain = [];
            name = name.toLowerCase();
            shop.forEach(function(element) {
                if(element.discname.toLowerCase().indexOf(name) !== -1){
                    cont++;
                    arrayMain.push(element);
                }
            });
            if(cont == 1){
                location.href = '#/shop/'+arrayMain[0].discid;
            }else if(cont > 1) {
                localStorage.setItem("name", name);
                location.href = '#/shop';
            }else {
                toastr.error('That product doesnt exists', 'Error');
            }
        }

    }]);
