musicpla.controller('cartCtrl', function ($scope, $uibModal, services, cart, $timeout, $route) {
	var price = 0;
	var oldID = 0;
	var count = 0;
	var cont = 0;
	var originPrice = [];
	var produts = [];
	var total  = 0;
	var prods = localStorage.getItem("cart");
	if(prods !== null){
		$scope.cartV = true;
		prods = prods.split(",");
		prods.forEach(function(element) {
			cart.forEach(function(element2){
				if(element === element2.discid){
					originPrice.push(element2.discprice);
					element2.discdesc = 1;
					produts.push(element2);
					total = total + parseFloat(element2.discprice);
				}
			});
		});
		$scope.prodsShop = produts;
		$scope.tric = true;
		$scope.tricV = false;
		$scope.total = total;
	}else {
		$scope.cartV = false;
	}

	$scope.deleteItem = function(id) {
		var cont = 0;
		prods.forEach(function(element3) {
			cont++;
			if(id === element3){
				var position = cont - 1;
				prods.splice(position, 1);
				localStorage.setItem("cart", prods);
				$route.reload();
			}
		});
	}

	$scope.valorr = function(valor, id) {
		total = 0;
		var num = 0;
		if(oldID !== id) {
			count = 0;
			cont++;
			if(cont === 2){
				$scope.tric = false;
				$scope.tricV = true;
			}
		}
		for (var i in produts) {
			for(var j in originPrice) {
				if(produts[i].discid === id){
					produts[i].discdesc = valor
					oldID = produts[i].discid;
					if(count > 0){
						produts[i].discprice = price;
					}
					if(parseFloat(originPrice[j]) === parseFloat(produts[i].discprice)){
						count++;
						price = parseFloat(originPrice[j]);
						num = num + parseFloat(originPrice[j]);
						num = num * valor;
					}
				}
			}
		}
		for (var x in cart) {
			if(cart[x].discid === id){
				cart[x].discprice = num;
				prods.forEach(function(element) {
					cart.forEach(function(element2){
						if(element === element2.discid){
							total = total + parseFloat(element2.discprice);
						}
					});
				});
				$scope.total = total;
			}
		}
	}

	$scope.checkout = function() {
        var modalInstance = $uibModal.open({
            animation: 'true',
            templateUrl: 'frontend/modules/cart/view/modalCart.view.html',
            controller: 'modalCartCtrl',
            size: "md",
            resolve: {
        	    prods: function () {
        	    	return produts;
        	    },
        	    profile: function(EncodeDecodeService) {
        	    	return EncodeDecodeService.InfoProfToken();
        	    }
        	}
        });
	}
});

musicpla.controller('modalCartCtrl', function ($scope, $uibModalInstance, services, $timeout, toastr, loginService, menuService, prods, profile) {
	var user = "";
	if(!localStorage.getItem("token")){
		$scope.noSingIn = true;
		$scope.SingIn = false;
	}else {
		$scope.SingIn = true;
		$scope.noSingIn = false;
	}

    $scope.register = function () {
        location.href = '#/login';
        $uibModalInstance.dismiss('cancel');
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

	$scope.SumbitLoginCart = function () {
		var data = {"userName": $scope.login.userName, "Password": $scope.login.Password};
		services.post('login', 'access', data).then(function (response) {
			if(response.success){
				user = $scope.login.userName;
				loginService.SetCredentials(response.data[0]);
				setTimeout(function(){ menuService.LoadMenu(); }, 1500);
				toastr.success('Welcome ' + $scope.login.userName, 'Succes');
				$scope.SingIn = true;
				$scope.noSingIn = false;
			}else{
				var json = JSON.parse(response);
				if(json === "error") {
					toastr.error('The username or the password was wrong', 'Error');
				}else if(json === "error2"){
					toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
				}else if(json === "error1") {
					toastr.error('You account are not active checkout your email, please', 'Error');
				}
			}
		});
	};

	$scope.shop = function () {
		var succes = false;
		if(user === ""){
			user = profile[0].user;
		}
		for (var  i in prods) {
			var prod = prods[i].discid;
			var price = prods[i].discprice;
			var qty = prods[i].discdesc;
			var data = {"user": user, "qty": qty, "prod": prod, "price": price};
			services.post('cart', 'shop', data).then(function (response) {
				var json = JSON.parse(response);
				if (json === true) {
					succes = true;
					$uibModalInstance.dismiss('cancel');
					localStorage.removeItem("cart");
				}
			});
		}
		$timeout(function(){if(succes === true) {
			toastr.success('Enjoy the albums!', 'Succes');
			location.href = '#/'; 		
		}}, 1000);
	}
});