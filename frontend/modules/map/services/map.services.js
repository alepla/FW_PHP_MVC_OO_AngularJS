musicpla.factory("prods_map", ['$rootScope',
function ($rootScope) {
    var service = {};
    service.loadMap = loadMap;
    service.marcar = marcar;
    return service;

    function loadMap(arrArguments, $rootScope, idSelect, cord) {
        var idSelect = idSelect;
        navigator.geolocation.getCurrentPosition(showPosition, showError);

        function showPosition(position){
            if(cord){
                lat = cord.lat;
                lon = cord.long;
            }else {
                lat = position.coords.latitude;
                lon = position.coords.longitude;
            }
            latlon = new google.maps.LatLng(lat, lon);
            albumMap = document.getElementById('albumMap');

            var myOptions = {
                center: latlon, zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            var map = new google.maps.Map(document.getElementById("albumMap"), myOptions);
            //var marker = new google.maps.Marker({position: latlon, map: map, title: "You are here!"});

            $rootScope.map = map;
            if (idSelect !== null) {
                arrArguments.forEach(function(element) {
                    var id = element.discid;
                    if(id.indexOf(idSelect) === 0){
                        for (var i = 0; i < arrArguments.length; i++) {
                            marcar(map, arrArguments[i], $rootScope, idSelect);
                        }
                    }else {
                        for (var i = 0; i < arrArguments.length; i++) {
                            marcar(map, arrArguments[i], $rootScope);
                        }
                    }
                });
            }
        }

        function showError(error){
            switch (error.code){
                case error.PERMISSION_DENIED:
                    $rootScope.demo = "Denegada la peticion de Geolocalización en el navegador.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    $rootScope.demo = "La información de la localización no esta disponible.";
                    break;
                case error.TIMEOUT:
                    $rootScope.demo = "El tiempo de petición ha expirado.";
                    break;
                case error.UNKNOWN_ERROR:
                    $rootScope.demo = "Ha ocurrido un error desconocido.";
                    break;
            }
        }
    }

    function marcar(map, prod, $rootScope, idSelect) {
        var latlon = new google.maps.LatLng(prod.lat, prod.lng);
        var marker = new google.maps.Marker({position: latlon, map: map, title: prod.discname, animation: null});

        marker.set('id', prod.discid);
        marker.set('latlon', latlon);

        var infowindow = new google.maps.InfoWindow({
            content: '<img class="img_mark" src="'+prod.discpic+'"/><p>'+prod.discname+'</p><p>'+prod.artist+'</p><p>Price:'+prod.discprice+'.00$</p><a href="#/shop/'+prod.discid+'">More info</a>'
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        $rootScope.markers.push(marker);

        if(idSelect){
            if(idSelect === marker.id){
                infowindow.open(map, marker);
            }
        }
    }
}]);