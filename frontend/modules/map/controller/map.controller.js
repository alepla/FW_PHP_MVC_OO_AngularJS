musicpla.controller('mapCtrl', function ($scope, services, map, prods_map) {
	$scope.markers = [];
	$scope.products = map;
	prods_map.loadMap($scope.products, $scope);

	$scope.viewAlbum = function(idSelect) {
		prods_map.loadMap($scope.products, $scope, idSelect);
	};

	services.get('map', 'loadCitys').then(function (response) {
		$scope.citys = response;
	});

	$scope.SelectedCity = function() {
		var city = $scope.selectedCity.nm;
		services.get('map', 'autocomplete', city).then(function (response) {
			var cord = response;
			var idSelect = 0;
			prods_map.loadMap($scope.products, $scope, idSelect, cord);
		});
	}
});