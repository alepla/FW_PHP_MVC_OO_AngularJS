musicpla.controller('mainCtrl', function ($scope, services, main, toastr, CommonService) {
	var count = 0;
	$scope.loadMore = true;
	$scope.albums = main;
	$scope.products = [];
	$scope.prods = main;
    $scope.currentPage = 1;
	$scope.products = $scope.prods.slice(0, 3);

	$scope.seeMore = function() {
		count++;
		if (count == 1){
			$scope.products = $scope.prods.slice(0, 6);
		}else if(count == 2) {
			$scope.products = $scope.prods.slice(0, 9);
			$scope.loadMore = false;
		}	
	};

	$scope.SelectedItem = function(){
		var shop = main;
		if($scope.selectedAlbums.discname){
			var name = $scope.selectedAlbums.discname;
		}else {
			var name = $scope.selectedAlbums;
		}
		CommonService.autocompleteDetails(name, shop);
	};
});