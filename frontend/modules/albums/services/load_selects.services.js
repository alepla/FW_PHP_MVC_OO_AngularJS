musicpla.factory("load_info", ['services', '$q', function (services, $q) {
    var service = {};
    service.load_artists = load_artists;
    service.load_genres = load_genres;
    service.load_subgenres = load_subgenres;
    return service;

    function load_artists() {
        var deferred = $q.defer();
        services.get("albums", "load_artists").then(function (response) {
            if (response === 'error') {
                deferred.resolve({ success: false, data: "errorLoadArtists" });
            }else {
                deferred.resolve({ success: true, data: response });
            }
        });
        return deferred.promise;
    };
    
    function load_genres() {
        var deferred = $q.defer();
        services.get("albums", "load_genres").then(function (response) {
            if (response === 'error') {
                deferred.resolve({ success: false, data: "errorLoadGenres" });
            } else {
                deferred.resolve({ success: true, data: response });
            }
        });
        return deferred.promise;
    };
    
    function load_subgenres(genre) {
        var deferred = $q.defer();
        services.get("albums", "load_subgenres", genre).then(function (response) {
            if (response === 'error') {
                deferred.resolve({ success: false, data: "errorLoadSubgenres" });
            } else {
                deferred.resolve({ success: true, data: response });
            }
        });
        return deferred.promise;
    };
}]);
