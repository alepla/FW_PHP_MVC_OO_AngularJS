musicpla.controller('musicCtrl', function ($scope, services, toastr, load_info) {
  $scope.createV = true;
  $scope.listV = false;
  load_info.load_artists().then(function (response) {
    if(response.success){
        $scope.artists = response.data.topartists.artist;
    }else{
      toastr.error('Somthing wrong was happened', 'Error');
    }
  });

  $scope.selectArtist = function () {
    load_info.load_genres().then(function (response) {
      if (response.success) {
        $scope.genres = response.data.genre;
      }else {
        toastr.error('Somthing wrong was happened', 'Error');
      }
    });
  }

  $scope.selectGenre = function () {
    var idGenre = $scope.create.genre.id;
    load_info.load_subgenres(idGenre).then(function (response) {
      if (response.success) {
        $scope.subgenres = response.data.subgenre;
      }else {
        toastr.error('Somthing wrong was happened', 'Error');
      }
    });
  }

  $scope.back = function () {
    $scope.createV = true;
    $scope.listV = false; 
  }

    $scope.create = {
      idAlbum: "",
      nameAlbum: "",
      priceAlbum: "",
      artist: "",
      genre: "",
      subgenre: "",
      descAlbum: "",
      typeAlbum: "",
      valorationAlbum: ""
    };

    $scope.SubmitAlbum = function () {
      var typeAlbum = "";
      var validResult = true;

      if($scope.create.CD) {
        typeAlbum = typeAlbum + $scope.create.CD + ' ';
      }
      if($scope.create.Vinyl) {
        typeAlbum = typeAlbum + $scope.create.Vinyl + ' ';
      }
      if($scope.create.Mp3) {
        typeAlbum = typeAlbum + $scope.create.Mp3 + ' ';
      }
      if($scope.create.Cassete) {
        typeAlbum = typeAlbum + $scope.create.Cassete;
      }
      $scope.create.format = typeAlbum;

      if(typeAlbum == ""){
        validResult = false;
        toastr.error('The type of album is needed', 'Error');
      }
      if($scope.create.valoration == null){
        validResult = false;
        toastr.error('The valoration is needed', 'Error');
      }
      if($scope.create.subgenre.subgenre == null){
        validResult = false;
        toastr.error('The artist, the genre and subgenre is needed', 'Error');
      }

      var dateEntry = $.datepicker.formatDate('dd-mm-yy', $scope.create.dtEntry);
      $scope.create.dateE = dateEntry;
      var dateExpiration = $.datepicker.formatDate('dd-mm-yy', $scope.create.dtExpiration);
      $scope.create.dateEx = dateExpiration;

      if(validResult) {
        var data = {"idAlbum": $scope.create.idAlbum, "nameAlbum": $scope.create.nameAlbum, "priceAlbum": $scope.create.priceAlbum,
        "dateEntry": dateEntry, "dateExpiration": dateExpiration,
        "artist": $scope.create.artist.name, "genre": $scope.create.genre.nombre, "subgenre": $scope.create.subgenre.subgenre,
        "descAlbum": $scope.create.descAlbum, "typeAlbum": typeAlbum, "valorationAlbum": $scope.create.valoration};
        var data_album_json = JSON.stringify(data);
        services.post('albums', 'alta_albums', data_album_json).then(function (response) {
          if (response.success) {
              $scope.create.img = response.img;
              toastr.success('The album has created', 'Succes');
              $scope.createV = false;
              $scope.listV = true;
          } else {
            if(response.error.discid) {
              toastr.error(response.error.discid, 'Error');
            }
            if(response.error.discname) {
              toastr.error(response.error.discname, 'Error');
            }
            if(response.error.discprice) {
              toastr.error(response.error.discprice, 'Error');
            }
            if(response.error.date_entry) {
              toastr.error(response.error.date_entry, 'Error');
            }
            if(response.error.date_expiration) {
              toastr.error(response.error.date_expiration, 'Error');
            }
            if(response.error.format) {
              toastr.error(response.error.format, 'Error');
            }
            if(response.error.artist) {
              toastr.error(response.error.artist, 'Error');
            }
            if(response.error.genre) {
              toastr.error(response.error.genre, 'Error');
            }
            if(response.error.subgenre) {
              toastr.error(response.error.subgenre, 'Error');
            }
            if(response.error.valoration) {
              toastr.error(response.error.valoration, 'Error');
            }
            if(response.error.discdesc) {
              toastr.error(response.error.discdesc, 'Error');
            }
            if(response.error_prodpic) {
              toastr.error(response.error_prodpic, 'Error');
            }
          }
        });
      }
    };

  $scope.dropzoneConfig = {
    'options': {
      'url': 'backend/index.php?module=albums&function=upload',
      addRemoveLinks: true,
      maxFileSize: 1000,
      dictResponseError: "An error has occurred on the server",
      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
    },
    'eventHandlers': {
      'sending': function (file, formData, xhr) {},
      'success': function (file, response) {
        response = JSON.parse(response);
        if (response.result) {
          $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
          $('.msg').animate({'right': '300px'}, 300);
        }else {
          $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
          $('.msg').animate({'right': '300px'}, 300);
        }
      },
    'removedfile': function (file, serverFileName) {
      if (file.xhr.response) {
          $('.msg').text('').removeClass('msg_ok');
          $('.msg').text('').removeClass('msg_error');
      }
    }
  }};

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[3];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
});