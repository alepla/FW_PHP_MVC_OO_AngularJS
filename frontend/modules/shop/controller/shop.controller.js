musicpla.controller('shopCtrl', function ($scope, $uibModal, services, shop, $route, $location, toastr, CommonService, $rootScope, EncodeDecodeService, totalRating, $timeout) {
    var prods = [];
    var cart = [];
    var unique = [];
	$scope.albums = shop;
	autocomplete();
	function autocomplete(){
		if(localStorage.getItem("name")){
			var arrayShop = [];
			var name = localStorage.getItem("name");
			name = name.toLowerCase();
			shop.forEach(function(element) {
				if(element.discname.toLowerCase().indexOf(name) !== -1){
					arrayShop.push(element);
				}
			});
			if(name != null){
				$scope.products = [];
				$scope.pros = arrayShop;
			    $scope.currentPage = 1;
				$scope.products = $scope.pros.slice(0, 4);
				$scope.filtro = name;
			}	
		}else {
			$scope.products = [];
			$scope.pros = shop;
		    $scope.currentPage = 1;
			$scope.products = $scope.pros.slice(0, 4);
            shop.forEach(function(element) {
                var count = 0;
                var media = 0;
                totalRating.forEach(function(element2) {
                    if(element.discid === element2.idProd){
                        count++;
                        media = media + parseFloat(element2.rating);
                        media = media / count;
                        element.valoration = count;
                        element.genre = media;
                    }else {
                        element.valoration = count;
                        element.genre = media;
                    }
                });
            });
		}
		localStorage.removeItem("name");
		name = "";
	}

    if(localStorage.getItem("token")) {
        $scope.WishV = true;
        $scope.starsV = true;
    }else {
        $scope.WishV = false;
        $scope.starsV = false;
    }
	
	$scope.ChangePage = function() {
	  	var startPos = ($scope.currentPage - 1) * 4;
	  	$scope.products = $scope.pros.slice(startPos, startPos + 4);
	};

	$scope.openModal = function (id) {
		var info = id;
        var modalInstance = $uibModal.open({
            animation: 'true',
            templateUrl: 'frontend/modules/shop/view/modalShop.view.html',
            controller: 'modalCtrl',
            size: "lg",
            resolve: {
                infoProd: function (services) {
   		            return services.get('shop', 'idProduct', info);
        	    }
        	}
        });
    };

    $scope.addToCart = function(id){
    	if(prods.length === 0){
    		cart = localStorage.getItem("cart");
    		if(cart !== null){
    			prods = cart.split(",");
    		}
    	}
    	prods.push(id);

    	for(var i = 0; i < prods.length; i++){
    		if(unique.indexOf(prods[i]) == -1){
    			unique.push(prods[i]);
    		}
    	}
    	localStorage.setItem("cart", unique);
        toastr.success('Added to cart', 'Success');
    };

    $scope.addToWishList = function(id){
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id};
            services.post('shop', 'wishList', data).then(function (response) {
                toastr.success('Added to my wish list', 'Success');
            });
        });
    }

    $scope.SelectedItem = function(){
    	if($scope.selectedAlbums.discname){
            var name = $scope.selectedAlbums.discname;
        }else {
            var name = $scope.selectedAlbums;
        }
    	CommonService.autocompleteDetails(name, shop)
    	autocomplete();
	};

    $scope.FiveStars = function(id) {
        var rating = 5;
        var keepGoing = true;
        var validation = "";
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id, "rating": rating};
            totalRating.forEach(function(element2) {
                if(keepGoing) {
                    if(element2.token === data.token && element2.idProd === data.id){
                        keepGoing = false;
                        toastr.error('Nooo', 'Error');
                    }
                }
            });
            $timeout(function(){
                if(keepGoing === true){
                    console.log(validation);
                    services.post('shop', 'Rating', data).then(function (response) {
                        toastr.success('Ratted', 'Success');
                    });
                }
            }, 1000);
        });
    }

    $scope.FourStars = function(id) {
        var rating = 4;
        var keepGoing = true;
        var validation = "";
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id, "rating": rating};
            totalRating.forEach(function(element2) {
                if(keepGoing) {
                    if(element2.token === data.token && element2.idProd === data.id){
                        keepGoing = false;
                        toastr.error('Nooo', 'Error');
                    }
                }
            });
            $timeout(function(){
                if(keepGoing === true){
                    console.log(validation);
                    services.post('shop', 'Rating', data).then(function (response) {
                        toastr.success('Ratted', 'Success');
                    });
                }
            }, 1000);
        });
    }

    $scope.ThreeStars = function(id) {
        var rating = 3;
        var keepGoing = true;
        var validation = "";
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id, "rating": rating};
            totalRating.forEach(function(element2) {
                if(keepGoing) {
                    if(element2.token === data.token && element2.idProd === data.id){
                        keepGoing = false;
                        toastr.error('Nooo', 'Error');
                    }
                }
            });
            $timeout(function(){
                if(keepGoing === true){
                    console.log(validation);
                    services.post('shop', 'Rating', data).then(function (response) {
                        toastr.success('Ratted', 'Success');
                    });
                }
            }, 1000);
        });
    }

    $scope.TwoStars = function(id) {
        var rating = 2;
        var keepGoing = true;
        var validation = "";
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id, "rating": rating};
            totalRating.forEach(function(element2) {
                if(keepGoing) {
                    if(element2.token === data.token && element2.idProd === data.id){
                        keepGoing = false;
                        toastr.error('Nooo', 'Error');
                    }
                }
            });
            $timeout(function(){
                if(keepGoing === true){
                    console.log(validation);
                    services.post('shop', 'Rating', data).then(function (response) {
                        toastr.success('Ratted', 'Success');
                    });
                }
            }, 1000);
        });
    }

    $scope.OneStars = function(id) {
        var rating = 1;
        var keepGoing = true;
        var validation = "";
        EncodeDecodeService.Decode(localStorage.getItem("token")).then(function (response){
            var data = {"token": response.data, "id": id, "rating": rating};
            totalRating.forEach(function(element2) {
                if(keepGoing) {
                    if(element2.token === data.token && element2.idProd === data.id){
                        keepGoing = false;
                        toastr.error('Nooo', 'Error');
                    }
                }
            });
            $timeout(function(){
                if(keepGoing === true){
                    console.log(validation);
                    services.post('shop', 'Rating', data).then(function (response) {
                        toastr.success('Ratted', 'Success');
                    });
                }
            }, 1000);
        });
    }
});

musicpla.controller('modalCtrl', function ($scope, $uibModalInstance, services, $timeout, infoProd) {
	$scope.infoProds = infoProd.album;
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

musicpla.controller('detailsCtrl', function ($scope, services, infoProd) {
	$scope.infoProds = infoProd.album;
});
  