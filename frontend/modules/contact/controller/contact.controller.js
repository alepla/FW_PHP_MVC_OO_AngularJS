musicpla.controller('contactCtrl', function ($scope, services, toastr) {
    $scope.contact = {
        name_contact: "",
        email_contact: "",
        subject_contact: "",
        message_contact: ""
    };

    $scope.SubmitContact = function () {
        var data = {"name_contact": $scope.contact.name_contact, "email_contact": $scope.contact.email_contact, 
        "subject_contact": $scope.contact.subject_contact, "message_contact": $scope.contact.message_contact};
        var data_contact_json = JSON.stringify(data);
        services.post('contact', 'send_contact', data_contact_json).then(function (response) {
            if (response == 'okok') {
                toastr.success('In a period of 24h we answer your question!', 'Succes');
            } else {
                toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
            }
        });
    };
});