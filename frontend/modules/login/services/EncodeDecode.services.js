musicpla.factory("EncodeDecodeService", ['MethodsService', '$q', 'services', function (MethodsService, $q, services) {
	var service = {};
    service.Encode = Encode;
    service.Decode = Decode;
    service.InfoProfToken = InfoProfToken;
    service.InfoProfUserID = InfoProfUserID;
    service.InfoWishList = InfoWishList;
    service.InfoRatings = InfoRatings;
    return service;

    function Encode(token) {
    	var deferred = $q.defer();
    	MethodsService.btoaEncode(token).then(function (response){
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    }

    function Decode(token) {
    	var deferred = $q.defer();
    	MethodsService.btoaDecode(token).then(function (response){
    		deferred.resolve(response);
    	});
    	return deferred.promise;
    }

    function InfoProfToken() {
    	var deferred = $q.defer();
    	var token = localStorage.getItem("token");
    	var consulta = ""
    	Decode(token).then(function (response){
            consulta = services.get('login', 'selectType', response.data);
            deferred.resolve(consulta);
        });
        return deferred.promise;
    }

    function InfoProfUserID() {
    	var deferred = $q.defer();
    	var userID = localStorage.getItem("userID");
    	var consulta = ""
    	Decode(userID).then(function (response){
            consulta = services.get('login', 'selectTypeSocial', response.data);
            deferred.resolve(consulta);
        });
        return deferred.promise;
    }

    function InfoWishList() {
        var deferred = $q.defer();
        var token = localStorage.getItem("token");
        var consulta = ""
        Decode(token).then(function (response){
            consulta = services.get('login', 'selectWishList', response.data);
            deferred.resolve(consulta);
        });
        return deferred.promise;
    }

    function InfoRatings() {
        var deferred = $q.defer();
        var token = localStorage.getItem("token");
        var consulta = ""
        Decode(token).then(function (response){
            consulta = services.get('login', 'selectInfoRatings', response.data);
            deferred.resolve(consulta);
        });
        return deferred.promise;
    }
}]);