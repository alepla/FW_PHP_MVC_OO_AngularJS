musicpla.factory("socialService", ['services', '$q', 'loginService', 'menuService', 'toastr', '$timeout', function (services, $q, loginService, menuService, toastr, $timeout) {
	var service = {};
    service.FbSignin = FbSignin;
    service.TwSignin = TwSignin;
    service.GoSignin = GoSignin;
    service.loadLogin = loadLogin;

	var config = {
		apiKey: "AIzaSyA5BfauROZx1KD_sQBiBl_FEaTUrF1iQes",
		authDomain: "music-e6c85.firebaseapp.com",
		databaseURL: "https://music-e6c85.firebaseio.com",
		projectId: "music-e6c85",
		storageBucket: "music-e6c85.appspot.com",
		messagingSenderId: "112671914694"
	};
	firebase.initializeApp(config);

	var provider = new firebase.auth.FacebookAuthProvider();
	var authService = firebase.auth();

	var providert = new firebase.auth.TwitterAuthProvider();

	var providerg = new firebase.auth.GoogleAuthProvider();
	providerg.addScope('email');

	return service;

	function FbSignin(){
		var deferred = $q.defer();
	    authService.signInWithPopup(provider).then(function(result) {
	        var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        	var social_JSON = JSON.stringify(data);
        	deferred.resolve({ success: true, data: data });
	    });
	    return deferred.promise;
	}

	function TwSignin(){
		var deferred = $q.defer();
  		authService.signInWithPopup(providert).then(function(result) {
        	var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        	var social_JSON = JSON.stringify(data);
        	deferred.resolve({ success: true, data: data });
        });
        return deferred.promise;
	}

	function GoSignin(){
		var deferred = $q.defer();
  		authService.signInWithPopup(providerg).then(function(result) {
        	var data = {"user": result.user.uid, "name": result.user.displayName, "email": result.user.email, "avatar": result.user.photoURL};
        	var socialJSON = JSON.stringify(data);
        	deferred.resolve({ success: true, data: data });
    	});
    	return deferred.promise;
	}

	function loadLogin(response) {
		if(response.success === true){
			var social_json = response.data;
			services.post('login', 'social', social_json).then(function (response) {
				if(response.success){
					loginService.SetCredentialsSocial(response.data.user);
					menuService.LoadMenuSocial();
					toastr.success('Welcome!', 'Succes');
					$timeout(function(){ location.href = '#/profile'; }, 3000);
				}else {
					toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
				}
			});
		}else {
			toastr.error('Somthing wrong was happened', 'Error');
		}
	}
}]);