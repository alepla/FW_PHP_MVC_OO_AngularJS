musicpla.factory("loginService", ['EncodeDecodeService', function (EncodeDecodeService) {
    var service = {};
    service.SetCredentials = SetCredentials;
    service.SetCredentialsSocial = SetCredentialsSocial;
    return service;

    function SetCredentials(token) {
        EncodeDecodeService.Encode(token.token).then(function (response){
            var encodedToken = response.data;
            localStorage.setItem("token", encodedToken);
        });
    }

    function SetCredentialsSocial(userID) {
        EncodeDecodeService.Encode(userID).then(function (response){
            var encodeduserID = response.data;
            localStorage.setItem("userID", encodeduserID);
        });
    }
}]);
