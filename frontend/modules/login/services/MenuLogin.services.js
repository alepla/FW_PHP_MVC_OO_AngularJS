musicpla.factory("menuService", ['services', '$rootScope', '$timeout', 'toastr', 'EncodeDecodeService', function (services, $rootScope, $timeout, toastr, EncodeDecodeService) {
    var service = {};
    service.LoadMenu = LoadMenu;
    service.LoadMenuSocial = LoadMenuSocial;
    service.logout = logout;
    service.type = type;
    return service;

    function LoadMenu() {
    	if(window.localStorage) {
    		var token = localStorage.getItem("token");
            if(token !== null) {
                EncodeDecodeService.Decode(token).then(function (response){
                    token = response.data;
                    services.get('login', 'selectType', token).then(function (response) {
                        type(response);
                    });
                });
            }else {
                $rootScope.loginV = true;
            }
    	}
    }

    function LoadMenuSocial() {
        if(window.localStorage) {
            var userID = localStorage.getItem("userID");
            if(userID !== null) {
                EncodeDecodeService.Decode(userID).then(function (response){
                    userID = response.data;
                    services.get('login', 'selectTypeSocial', userID).then(function (response) {
                        type(response);
                    });
                });
            }else {
                $rootScope.loginV = true;
            }
        }
    }

    function logout() {
        localStorage.removeItem("token");
        localStorage.removeItem("userID");
        location.href=".";
    }

    function type(response){
        if(response[0].type == "admin"){
            $rootScope.loginV = false;
            $rootScope.crudV = true;
            $rootScope.profileV = true;
            $rootScope.logoutV = true;
        }else if(response[0].type == "client"){
            $rootScope.loginV = false;
            $rootScope.crudV = false;
            $rootScope.profileV = true;
            $rootScope.logoutV = true;
        }else{
            toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
            location.href = '.';
        }
    }
}]);
