musicpla.factory("load_citys", ['services', '$q', function (services, $q) {
    var service = {};
    service.load_citys = load_citys;
    return service;

    function load_citys() {
        var deferred = $q.defer();
        services.get('login', 'loadCitys').then(function (response) {
            deferred.resolve({ success: true, data: response });    
        });
        return deferred.promise;
    };
}]);