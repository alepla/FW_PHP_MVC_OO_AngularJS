musicpla.controller('menuController', function($scope, services, menuService) {
	menuService.LoadMenu();
	menuService.LoadMenuSocial();

	$scope.logout = function () {
		menuService.logout();
	};
});

musicpla.controller('loginCtrl', function ($scope, services, toastr, loginService, menuService, $timeout, socialService, $rootScope) {
	$scope.btnRegister = function () {
		location.href = '#/login/register';
	};

	$scope.btnRecover = function () {
		location.href = '#/login/recover';
	};

	$scope.login = {
		userName: "",
		Password: ""
	};

	$scope.SumbitLogin = function () {
		var data = {"userName": $scope.login.userName, "Password": $scope.login.Password};
		services.post('login', 'access', data).then(function (response) {
			if(response.success){
				loginService.SetCredentials(response.data[0]);
				menuService.LoadMenu();
				toastr.success('Welcome ' + $scope.login.userName, 'Succes');
				$timeout(function(){ location.href = '#/profile/'; }, 3000);
			}else{
				var json = JSON.parse(response);
				if(json === "error") {
					toastr.error('The username or the password was wrong', 'Error');
				}else if(json === "error2"){
					toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
				}else if(json === "error1") {
					toastr.error('You account are not active checkout your email, please', 'Error');
				}
			}
		});
	};

	$scope.SubmitLoginFb = function () {
		socialService.FbSignin().then(function (response) {
			socialService.loadLogin(response);
		});
	};

	$scope.SubmitLoginTw = function () {
		socialService.TwSignin().then(function (response) {
			socialService.loadLogin(response);
		});
	};

	$scope.SubmitLoginGo = function () {
		socialService.GoSignin().then(function (response) {
			socialService.loadLogin(response);
		});
	};
});

musicpla.controller('registerCtrl', function ($scope, services, toastr, $timeout) {
    $scope.register = {
        nameUser: "",
        passwordUser: "",
        repasswordUser: "",
        emailUser: ""
    };

    $scope.SubmitRegister = function () {
        var data = {"nameUser": $scope.register.nameUser, "passwordUser": $scope.register.passwordUser, 
        "repasswordUser": $scope.register.repasswordUser, "emailUser": $scope.register.emailUser};
        var data_register_json = JSON.stringify(data);
        services.post('login', 'register_user', data_register_json).then(function (response) {
            if (response.success) {
                toastr.success('Now you are in Pla Music, checkout your email', 'Succes');
                $timeout(function(){ location.href = '#/main'; }, 3000);
            }else if(!response.success) {
            	if(response.error.user){
            		toastr.error(response.error.user, 'Error');
            	}else if(response.error.email) {
            		toastr.error(response.error.email, 'Error');
            	}
            }else {
                toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
            }
        });
    };
});

musicpla.controller('verifyCtrl', function ($scope, services, toastr, $route, $timeout) {
	var token = $route.current.params.token;
    services.put('login', 'verify', {"token": token}).then(function (response) {
    	var json = JSON.parse(response);
    	if(json == "good"){
    		toastr.success('Your account was verifyed succesfully', 'Succes');
            $timeout(function(){ location.href = '#/main'; }, 3000);
    	}else{
    		toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
    	}
    });
});

musicpla.controller('recoverCtrl', function ($scope, services, toastr, $timeout) {
	$scope.recover = {
		emailRec: ""
	};

	$scope.SubmitEmail = function () {
		var data = $scope.recover.emailRec;
		services.put('login', 'recover_pass', {"data": data}).then(function (response) {
			var json = JSON.parse(response);
            if (json == "good") {
                toastr.success('Checkout your email', 'Succes');
                $timeout(function(){ location.href = '#/main'; }, 3000);
            } else {
                toastr.error('Somthing wrong was happened in the process please try latter', 'Error');
            }
		});
	};
});

musicpla.controller('changepassCtrl', function ($scope, services, toastr, $route) {
	var token = $route.current.params.token;
	$scope.change = {
		password: "",
		passwordRepeat: ""
	};
	$scope.SubmitPasswords = function () {
		var data = {"password": $scope.change.password, "passwordRepeat": $scope.change.passwordRepeat, "token": token};
		var data_passwords_json = JSON.stringify(data);
		services.put('login', 'change_pass', data_passwords_json).then(function (response) {
			var json = JSON.parse(response);
			if(json == "good"){
				toastr.success('Your password has changed correctly', 'Succes');
                setTimeout(function(){ location.href = '#/main'; }, 3000);
			}else if(json == "error"){
				toastr.error('Something wrong was happened changing the password, try latter', 'Error');
			}else if(json == "error1"){
				toastr.error('Something wrong was happened with your email try again please', 'Error');
			}else if(json == "505"){
				location.href = '#/505';
			}else {
				toastr.error(json, 'Error');
			}
		});
	}
});

musicpla.controller('profileCtrl', function ($scope, services, toastr, $route, profile, load_citys, menuService, myWishList, cart, myRating) {
	menuService.LoadMenu();
	menuService.LoadMenuSocial();
	$scope.infoV = true;	
	$scope.updateInfoV = false;
	$scope.info = profile[0];
	var products = [];
	var ratings = [];

	if(localStorage.getItem("token")){
		if(myWishList.success === true){
			$scope.MyWishListV = true;
			myWishList.data.forEach(function(element) {
				cart.forEach(function(element2){
					if(element.idProd === element2.discid){
						products.push(element2);
						$scope.prodsWishList = products;
					}
				});
			});
		}else {
			$scope.MyWishListV = false;
		}
	}else {
		$scope.MyWishListV = false;
	}

	if(localStorage.getItem("token")){
		if(myRating.success === true){
			$scope.MyRatingV = true;
			myRating.data.forEach(function(element) {
				cart.forEach(function(element2){
					if(element.idProd === element2.discid){
						element2.valoration = element.rating;
						ratings.push(element2);
						$scope.prodsRated = ratings;	
					}
				});
			});
		}else {
			$scope.MyRatingV = false;
		}
	}else {
		$scope.MyRatingV = false;
	}

	$scope.updateProfile = function () {
		$scope.infoV = false;
		$scope.updateInfoV = true;

		load_citys.load_citys().then(function (response) {
		    if(response){
		        $scope.citys = response.data;
		    }else{
		      toastr.error('Somthing wrong was happened', 'Error');
		    }
	    });

	    $scope.update = {
	       	nameUser: "",
	       	lastnameUser: "",
	       	citys: "",
	       	date: ""
    	};

	    $scope.saveProfile = function () {
	    	var user = profile[0].user;
	    	var avatar = profile[0].avatar;
	    	var name = $scope.update.nameUser;
	    	var lastname = $scope.update.lastnameUser;
	    	var city = $scope.update.citys;
	    	var date = $scope.update.date;
	    	if(name === ""){
	    		name = profile[0].name;
	    	}
	    	if(lastname === ""){
	    		lastname = profile[0].lastname;
	    	}
	    	if(city === ""){
	    		city = profile[0].city;
	    	}else {
	    		city = $scope.update.citys.nm;
	    	}
	    	if(date === ""){
	    		date = profile[0].date_birthday;
	    	}
			var data = {"user": user,"nameUser": name, "lastnameUser": lastname, 
        	"city": city, "date": date, "avatar": avatar};
        	services.put('login', 'update_info_prof', data).then(function (response) {
        		if(response.success == false){
					toastr.error(response.img_profile, 'Error');
				}else {
					var json = JSON.parse(response);
					if(json == "good"){
						toastr.success('Your information has changed correctly', 'Succes');
		                setTimeout(function(){ $route.reload(); }, 2000);
					}else if(json == "error"){
						toastr.error('Something wrong was happened changing the password, try latter', 'Error');
					}else if(json == "error1"){
						toastr.error('That age is imposible', 'Error');
					}
				}
        	});
		}

	    $scope.cancelProfile = function () {
			$scope.infoV = true;	
			$scope.updateInfoV = false;
			$scope.info = profile[0];
		}
	};
	  $scope.dropzoneProfile = {
	    'options': {
	      'url': 'backend/index.php?module=login&function=upload',
	      addRemoveLinks: true,
	      maxFileSize: 1000,
	      dictResponseError: "An error has occurred on the server",
	      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
	    },
	    'eventHandlers': {
	      'sending': function (file, formData, xhr) {},
	      'success': function (file, response) {
	        response = JSON.parse(response);
	        if (response.result) {
	          $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
	          $('.msg').animate({'right': '300px'}, 300);
	        }else {
	          $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
	          $('.msg').animate({'right': '300px'}, 300);
	        }
	      },
	    'removedfile': function (file, serverFileName) {
	      if (file.xhr.response) {
	          services.get('login', 'delete').then(function (response) {
		        $('.msg').text('').removeClass('msg_ok');
		        $('.msg').text('').removeClass('msg_error');
	          });
	      }
	    }
	  }};
});