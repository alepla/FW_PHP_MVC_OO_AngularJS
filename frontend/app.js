var musicpla = angular.module('musicpla',['ngRoute', 'ui.bootstrap', 'toastr', 'ngAnimate']);
musicpla.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        // Home
        .when("/", {
            templateUrl: "frontend/modules/main/view/list_main.view.html", 
            controller: "mainCtrl",
            resolve: {
                main: function (services) {
                    return services.get('main', 'page');
                }
            }
        })

        // Contact
        .when("/contact", {templateUrl: "frontend/modules/contact/view/list_contact.view.html", controller: "contactCtrl"})

        .when("/shop", {
            templateUrl: "frontend/modules/shop/view/list_shop.view.html",
            controller: "shopCtrl",
            resolve: {
                shop: function (services) {
   		            return services.get('shop', 'prods');
        	    },
                totalRating: function (services) {
                    return services.get('shop', 'totalRating');
                }
        	}
   	    })

        .when("/shop/:id", {
            templateUrl: "frontend/modules/shop/view/details.view.html",
            controller: "detailsCtrl",
            resolve: {
                infoProd: function (services, $route) {
                    return services.get('shop', 'idProduct', $route.current.params.id);
                }
            }
        })

        .when("/albums", {templateUrl: "frontend/modules/albums/view/create_albums.view.html", controller: "musicCtrl"})

        .when("/map", {
            templateUrl: "frontend/modules/map/view/list_map.view.html", 
            controller: "mapCtrl",
            resolve: {
                map: function (services) {
                    return services.get('map', 'list_albums');
                }
            }})

        .when("/login", {templateUrl: "frontend/modules/login/view/signin.view.html", controller: "loginCtrl"})

        .when("/login/register", {templateUrl: "frontend/modules/login/view/register.view.html", controller: "registerCtrl"})

        .when("/login/verify/:token", {templateUrl: "frontend/modules/login/view/signin.view.html", controller: "verifyCtrl"})

        .when("/login/recover", {templateUrl: "frontend/modules/login/view/recover.view.html", controller: "recoverCtrl"})

        .when("/login/changepass/:token", {templateUrl: "frontend/modules/login/view/changepass.view.html", controller: "changepassCtrl"})

        .when("/profile", {
            templateUrl: "frontend/modules/login/view/profile.view.html", 
            controller: "profileCtrl",
            resolve: {
                profile: function (services, EncodeDecodeService) {
                    if(localStorage.getItem("token") !== null){
                        return EncodeDecodeService.InfoProfToken();
                    }else if(userID = localStorage.getItem("userID") !== null){
                        return EncodeDecodeService.InfoProfUserID();
                    }
                },
                myWishList: function (services, EncodeDecodeService) {
                    if(localStorage.getItem("token") !== null){
                        return EncodeDecodeService.InfoWishList();
                    }
                },
                cart: function (services) {
                    return services.get('cart', 'list_prods');
                },
                myRating: function (services, EncodeDecodeService) {
                    if(localStorage.getItem("token") !== null){
                        return EncodeDecodeService.InfoRatings();
                    }
                }
            }})

        .when("/cart", {
            templateUrl: "frontend/modules/cart/view/cart.view.html", 
            controller: "cartCtrl",
            resolve: {
                cart: function (services) {
                    return services.get('cart', 'list_prods');
                }
            }})

        // else 404
        .otherwise("/", {templateUrl: "frontend/modules/main/view/list_main.view.html", controller: "mainCtrl"});
}]);

musicpla.run(function($rootScope) {

})